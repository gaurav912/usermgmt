import csv
from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework import viewsets
from .models import UserDetail, Student
from rest_framework import status
from .serializers import UserSerializer, StudentSerializer, StudentCreateSerializer, StudentListSerializer
from .permissions import IsAdmin


class UserModelViewSet(viewsets.ModelViewSet):
    queryset = UserDetail.objects.all()
    serializer_class = UserSerializer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    permission_classes = [IsAuthenticated, IsAdmin]

    def get_permissions(self):
        get_list = []
        if self.action == 'list':
            get_list.append(IsAuthenticated)
        elif self.action == 'create':
            get_list.append(IsAdmin)
        return [permission() for permission in get_list]
    # serializer_class = StudentSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        s = StudentSerializer(queryset, many=True)
        data = {
            'data': s.data,
            'message': "Successfully retrieved the data"
        }
        return Response(data, status=status.HTTP_200_OK)

    def get_serializer_class(self):
        if self.action == 'list':
            return StudentListSerializer
        elif self.action == 'create':
            return StudentCreateSerializer


class UserDataApi(APIView):

    serializer_class = UserSerializer

    def get_serializer(self, queryset, many=True):
        return self.serializer_class(
            queryset,
            many=many,
        )

    def get(self, request, *args, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="export.csv"'

        serializer = self.get_serializer(
            UserDetail.objects.all(),
            many=True
        )
        header = UserSerializer.Meta.fields

        writer = csv.DictWriter(response, fieldnames=header)
        writer.writeheader()
        for row in serializer.data:
            writer.writerow(row)

        return response

    # def get(self, request):
    #     # name = request.GET.get('name')
    #     param = request.GET.get('name')
    #     userdata = UserDetail.objects.all()
    #     # if param.keys() == 'email':
    #     # # email = request.GET.get('email')
    #     #     user_data = UserDetail.objects.get(email=param.get('email'))
    #     # if param.keys() is None:
    #     #     qs = UserDetail.objects.all()
    #     #     users = {}
    #     #     for data in qs:
    #     #         users[data.id] = {'user': data.name, 'email': data.email}
    #     #     return Response(
    #     #         users
    #     #     )
    #     # else:
    #     #     return Response({
    #     #         'data': None
    #     #     })
    #     # return Response({
    #     #     # 'user': user_data.name,
    #     #     # 'email': user_data.email,
    #     #     # 'address': user_data.address,
    #     #     'users': users
    #     # })
    #     serializer = UserSerializer(userdata, many=True)
    #     return Response(serializer.data)

    # def post(self, request):
    #     name = request.data.get('name')
    #     email = request.data.get('email')
    #     address = request.data.get('address')
    #     user = UserDetail.objects.create(name=name, email=email, address=address)
    #     return Response({'id':user.id,'name': user.name, 'email': user.email, 'address': user.address}, status=status.HTTP_201_CREATED)

    def post(self, request):
        user_data = request.data
        password = user_data.pop('password')
        serializer = UserSerializer(data=user_data)
        if serializer.is_valid():
            v = serializer.validated_data
            user = UserDetail.objects.create(**v, password=password)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
