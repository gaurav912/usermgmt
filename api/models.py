from django.db import models


class UserDetail(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=200)
    address = models.CharField(max_length=200)
    password = models.CharField(max_length=100, default='')


class Student(models.Model):
    # name = models.CharField(max_length=50)
    roll_no = models.IntegerField()
    grade = models.CharField(max_length=10)
    user = models.ForeignKey(UserDetail, on_delete=models.CASCADE)

