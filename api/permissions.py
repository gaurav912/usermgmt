from rest_framework.permissions import BasePermission


class IsAdmin(BasePermission):

    def has_permission(self, request, view):
        if request.user.groups.all()[0].name == 'admin':
            return True
        return False
