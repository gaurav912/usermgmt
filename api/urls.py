from django.urls import path, include
from .views import UserDataApi, UserModelViewSet, StudentViewSet
from rest_framework.routers import DefaultRouter
from .models import UserDetail
from rest_framework.authtoken.views import obtain_auth_token

router = DefaultRouter()
router.register('users', UserModelViewSet)
router.register('students', StudentViewSet)
urlpatterns = [
    # path('users/', UserDataApi.as_view(), name='user_detail'),
    # path('users/', UserModelViewSet.as_view({'get': 'list', 'post': 'create'}), name='user_det'),
    # path('', include(router.urls))
    path('login/', obtain_auth_token, name='rest_login')
] + router.urls
