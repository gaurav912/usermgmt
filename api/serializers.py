from rest_framework import serializers
from .models import UserDetail, Student


class UserSerializer(serializers.ModelSerializer):
    # user = serializers.HyperlinkedIdentityField(view_name="api:students-detail")
    class Meta:
        model = UserDetail
        fields = [
            'id',
            'name',
            'email',
            'address',
        ]


class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = ['user', 'roll_no', 'grade']

    def to_representation(self, instance):
        r = super().to_representation(instance)
        r['user'] = UserSerializer(instance.user).data
        return r


class StudentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'


class StudentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'
